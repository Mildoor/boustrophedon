import time
start = time.time()
import Environments
import GUI
import Boustrophedon
import Create_graph
import Find_way
import Rotation
import Create_point
import tkinter as tk


rot = Rotation.Rotation("C:\\1. Složka - Miloš Cihlář\\Miloš Cihlář\\Programování\\PYTHON\\Boustrophedon\\File\\hranice.csv",
                        "C:\\1. Složka - Miloš Cihlář\\Miloš Cihlář\\Programování\\PYTHON\\Boustrophedon\\File\\prekazka.csv"
)

hranice = Environments.Environment(rot.node_edge_point, rot.node_barrier_point)

matrix = hranice.create_edge_and_barriers(rot.node_edge_point, hranice.list_of_bariers, hranice.matrix)

mc = Boustrophedon.Boustrophedon(matrix)    # v případě zpětné rotace odstranit rot.width a rot.height a smazat anitoraton v boustrophedon
matrix = mc.final_decomposition()

graph = Create_graph.Create_graph(matrix, mc.cell_number)
matrix_dependencies = graph.graph()

way = Find_way.Way(matrix_dependencies)
sequence = way.DFS_Algorithm()

print(sequence)

print()
final_point = Create_point.Create_point(matrix, sequence, rot.node_edge_point, rot.node_barrier_point)
points = final_point.find_point()

final_points = rot.antirotation(points)

print("RET POINT")
for  i in final_points:
    print(i)


end = time.time()
print("\n__________________________________________\n")
print("Bez simulace: ",(end - start))

height, width = hranice.proportions
root = tk.Tk()
app = GUI.MainWindow(root, height, width, matrix, points)
end2 = time.time()

print("Se simulací: ",(end2 - start))
print("\n__________________________________________\n")

app.mainloop()
