import tkinter as tk

class MainWindow(tk.Frame):

    def __init__(self, root, dimension_y, dimension_x, matrix, point, win_widh = 1000, win_height = 820):
        super().__init__(root)
        self.root = root
        self.__win_width = win_widh
        self.__win_height = win_height

        self.__matrix = matrix
        self.__point = point

        self.__edge_space = 1
        self.__barrier_space = 2
        self.__free_space = 0

        self.__dimension_x = int(dimension_x)
        self.__dimension_y = int(dimension_y)
        self.__win_label = []

        self.__set_window()
        self.__labels()
        self.__browse_matrix(self.__matrix)
        self.__browse_point(self.__point)
        self.root.update()

    def __set_window(self):
        self.root.title("Environment")
        self.root.geometry("%dx%d" % (self.__win_width, self.__win_height))
        self.root.resizable(0,0)

        for i in range(0, self.__dimension_x):
            self.root.columnconfigure(i, weight = 1)

        for i in range(0, self.__dimension_y):
            self.root.rowconfigure(i, weight = 1)

    def __labels(self):
        for i in range(0, self.__dimension_y):
            helplist = []
            for j in range(0, self.__dimension_x):
                helplist.append(0)

            self.__win_label.append(helplist)

        for y in range(0, self.__dimension_y):
            for x in range(0,self.__dimension_x):
                self.__win_label[y][x] = tk.Label(bg = "white") #
                self.__win_label[y][x].grid(column = x, row = y, sticky = tk.NSEW, padx = 1, pady = 1)

    def __set_color(self, dimension_y, dimension_x, color):
        self.__win_label[dimension_y][dimension_x].configure(bg = color)

    def __set_number(self, dimension_y, dimension_x, number):
        self.__win_label[dimension_y][dimension_x].config(text='{0}'.format(number-1))

    def __browse_matrix(self, matrix):
        for y in range(0, matrix.shape[0]):
            for x in range(0, matrix.shape[1]):
                if(matrix[y,x] == (self.__edge_space or self.__barrier_space)):
                    self.__set_color(y,x, "black")
                else:
                    self.__set_number(y,x, matrix[y,x])

    def __browse_point(self, point):
        for sublist in point:
            for list in sublist:
                self.__set_color(list[0],list[1], "red")
