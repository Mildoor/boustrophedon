import numpy as np
import csv

class Rotation():
    def __init__(self, input_environment_file, barrier_file):
        self.__file_environment = input_environment_file
        self.__file_barriers = barrier_file

        self.__node_edge_point = self.__load_nodes(self.__file_environment)
        self.__node_barrier_point = self.__load_nodes(self.__file_barriers)
        self.__height, self.__width = self.__size_of_matrix(self.__node_edge_point)

        self.__shift_x = 0
        self.__shift_y = 0
        self.__theta = 0

        self.__orientation = {
                "UpR": ((self.__node_edge_point[0][1] < self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] > self.__node_edge_point[1][0])),
                "R"  : ((self.__node_edge_point[0][1] < self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] == self.__node_edge_point[1][0])),
                "DwR": ((self.__node_edge_point[0][1] < self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] < self.__node_edge_point[1][0])),
                "D"  : ((self.__node_edge_point[0][1] == self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] < self.__node_edge_point[1][0])),
                "DwL": ((self.__node_edge_point[0][1] > self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] < self.__node_edge_point[1][0])),
                "L"  : ((self.__node_edge_point[0][1] > self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] == self.__node_edge_point[1][0])),
                "LUp": ((self.__node_edge_point[0][1] > self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] > self.__node_edge_point[1][0])),
                "Up" : ((self.__node_edge_point[0][1] == self.__node_edge_point[1][1]) and (self.__node_edge_point[0][0] > self.__node_edge_point[1][0])),
            }

        self.__theta = np.arctan2(np.absolute(self.__node_edge_point[0][0] - self.__node_edge_point[1][0]),(np.absolute(self.__node_edge_point[1][1] - self.__node_edge_point[0][1])))      # přidat souřadnice ze souboru rot.csv

        if(self.__orientation["UpR"]):
            self.__theta = np.pi/2 - self.__theta
        elif(self.__orientation["R"]):
            self.__theta = np.pi/2
        elif(self.__orientation["DwR"]):
            self.__theta = np.pi - ( np.pi/2 - self.__theta)
        elif(self.__orientation["D"]):
            self.__theta = np.pi
        elif(self.__orientation["DwL"]):
            self.__theta = np.pi + (np.pi/2 - self.__theta)
        elif(self.__orientation["L"]):
            self.__theta =  -np.pi/2
        elif(self.__orientation["LUp"]):
            self.__theta = - self.__theta
        elif(self.__orientation["Up"]):
            self.__theta = 0

        self.__node_edge_point = self.rotation(self.__node_edge_point)

        if(len(self.__node_barrier_point) > 0):
            self.__node_barrier_point = self.rotation(self.__node_barrier_point)

        self.__shift_y, self.__shift_x = self.__search_shift()
        self.__node_edge_point = self.__correction(self.__node_edge_point)
        self.__node_barrier_point = self.__correction(self.__node_barrier_point)

    @property
    def height(self):
        return self.__height

    @property
    def width(self):
        return self.__width

    @property
    def shift_x(self):
        return self.__shift_x
    @property
    def shift_y(self):
        return self.__shift_y
    @property
    def theta(self):
        return self.__theta

    @property
    def node_edge_point(self):
        return self.__node_edge_point

    @property
    def node_barrier_point(self):
        return self.__node_barrier_point


    def rotation(self, list_points):
        rot_list = []
        rot_matrix = np.array([[np.cos(self.__theta), -np.sin(self.__theta)],
                               [np.sin(self.__theta),  np.cos(self.__theta)]])

        for list in list_points:
            if(list != []):
                mc = (np.array([list])).T
                mc = (rot_matrix.dot(mc)).T
                mc = mc[0]                 # převedu na 1 rozměrné pole
                rot_list.append([mc[0],mc[1]])
            else:
                rot_list.append([])

        return rot_list


    def antirotation(self, list_points):
        rot_list = []
        rot_sublist = []
        rot_matrix = np.array([[np.cos(-self.__theta), -np.sin(-self.__theta)],
                               [np.sin(-self.__theta),  np.cos(-self.__theta)]])

        for sublist in list_points:
            for item in sublist:
                mc = (np.array([item[0] - self.__shift_y, item[1] - self.__shift_x])).T
                mc = (rot_matrix.dot(mc)).T
                rot_sublist.append([round(mc[0])/10,round(mc[1])/10])

            rot_list.append(rot_sublist)
            rot_sublist = []

        return rot_list

    def __search_shift(self):
        """
        Najde největší přesah souřadnic z self.__node_edge_point v xové a yové souřadnici a vráti je
        """
        shift_x = 0
        shift_y = 0
        for item in self.__node_edge_point:

            if(item[0] <= shift_y):
                shift_y = item[0]

            if(item[1] <= shift_x):
                shift_x = item[1]


        shift_x = round(np.absolute(shift_x)) + 1
        shift_y = round(np.absolute(shift_y)) + 1

        if(shift_x == 1):
            shift_x = 9999999999999
            for item in self.__node_edge_point:
                if(item[1] < shift_x):
                    shift_x = item[1]

            shift_x = -shift_x + 1

        if(shift_y == 1):
            shift_y = 9999999999999
            for item in self.__node_edge_point:
                if(item[0] < shift_y):
                    shift_y = item[0]

            shift_y = -shift_y + 1

        return shift_y , shift_x             # +1 je jistota

    def __correction(self, list_points):
        """
        Posune všechny souřadnice list_points o shift
        """
        list = []
        for item in list_points:
            if(item != []):
                list.append([(item[0] + self.__shift_y), (item[1] + self.__shift_x)])
            else:
                list.append([])
        return list


    def __load_nodes(self, soubor):
        """
        Vrátí souřadnice(uzly) ze souboru (přetypované na int): [[y1,x1] [y2,x2] ...]
        """
        list = []
        sublist = []
        with open(soubor) as csvnodes:
            readCSV = csv.reader(csvnodes, delimiter = ',')

            for row in readCSV:
                for element in row:
                    sublist.append(int(float(element)*10))          # jemnost 10*10 cm

                list.append(sublist)
                sublist = []

        return list

    def __size_of_matrix(self, node_edge_point):
        "Vrátí maximální rozměry matice podle zadaných vstupních souborů"
        width = 0
        height = 0

        for i in node_edge_point:
            if(i[0] >= height):
                height = i[0]

            if(i[1] >= width):
                width = i[1]

        # 2 proto, protože na stranách matice nesmí být překážka
        height = height + 2
        width = width + 2

        return int(round(height)), int(round(width))
