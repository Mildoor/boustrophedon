import numpy as np

class Create_graph():
    def __init__(self, matrix, cell_number):
        self.__matrix = matrix
        self.__cell_number = cell_number - 1

        self.__graph_matrix = self.__create_graph_matrix()
        self.__line_number = 0

        self.__line_one = []
        self.__line_two = []


    def __create_graph_matrix(self):
        return np.full((self.__cell_number, self.__cell_number), 0)

    def __create_dependence(self, cell_one, cell_two):
        self.__graph_matrix[cell_one - 2][cell_two - 2] = 1
        self.__graph_matrix[cell_two - 2][cell_one - 2] = 1

    def __get_next_lines(self):
        """
        Zvýší pozicí obou linií
        """
        self.__line_number = self.__line_number + 1

        if(self.__line_number >= self.__matrix.shape[1]):
            self.__line_one = self.__line_two
        else:
            self.__line_one = self.__matrix[:, self.__line_number - 1]
            self.__line_two = self.__matrix[:, self.__line_number]

    def graph(self):

        while(self.__line_number < self.__matrix.shape[1]-1):
            self.__get_next_lines()

            if(len(self.__line_one) >= len(self.__line_two)):
                iterator = len(self.__line_two)
            else:
                iterator = len(self.__line_one)

            for i in range(iterator):

                if(self.__line_one[i] == self.__line_two[i]):
                    continue
                elif(self.__line_one[i] == 1 or self.__line_two[i] == 1):
                    continue
                elif(self.__graph_matrix[self.__line_one[i]-2][self.__line_two[i]-2] == 1):
                    continue
                else:
                    self.__create_dependence(self.__line_one[i], self.__line_two[i])

        return self.__graph_matrix
