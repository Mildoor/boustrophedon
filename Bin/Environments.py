import numpy as np
import math
# na pozici [0,0] NESMÍ být překážka !!!!!!!!!!!!!!!!!!!!!!!
# na osách y = 0 a x = 0 nesmí být překážka
class Environment():
    """
    Vytvoří pole překážek a hranic prostředí, ve kterém se robot pohybuje
    Vstupem jsou 2 soubory (csv) se vstupními body(souřadnice v metrech) hranice a se vstupními body překážky
    -----> x width      [y,x]
    |
    |
    |
    y height
    """
    def __init__(self, node_edge_point, node_barrier_point):

        self.__edge_space = 1   # zastupuje hranici ve výsledné matici
        self.__free_space = 0   # zastupuje volný prostor ve výsledné matici
        self.__barrier_space = 2    # zastupuje překážku ve výsledné matici

        self.__list_of_barriers = self.__find_all_barriers(node_barrier_point)
        self.__number_of_barriers = len(self.__list_of_barriers)

        self.__height, self.__width = self.__size_of_matrix(node_edge_point)

        self.__matrix = self.__create_matrix(self.__height, self.__width)


    @property
    def list_of_bariers(self):
        return self.__list_of_barriers

    @property
    def number_of_bariers(self):
        return self.__number_of_barriers

    @property
    def proportions(self):
        return self.__height, self.__width

    @property
    def matrix(self):
        return self.__matrix


    def __find_all_barriers(self, barriers_list):
        """
        Vrátí 3 rozměrné pole s jednotlivými překážkami [ [[souranice1]] ,[[souranice2]], [[souradnice3]] , ..., [[souranice n]] ]
        """
        list = []
        sublist = []
        for i in barriers_list:
            if (i == []):
                list.append(sublist)
                sublist = []
            else:
                sublist.append(i)

        list.append(sublist) # připojí poslední barieru, protože není ukončena [] a musí byt appendovana mimo cyklus for, jinak se nezobrazí
        return list


    def __create_matrix(self, height, width):
        """
        Vrátí matici (s šířkou a výškou) naplněnou self.__free_space
        """
        matrix = np.full((height, width), self.__free_space)
        return matrix

    def __size_of_matrix(self, node_edge_point):
        "Vrátí maximální rozměry matice podle zadaných vstupních souborů"
        width = 0
        height = 0

        for i in node_edge_point:
            if(i[0] >= height):
                height = i[0]

            if(i[1] >= width):
                width = i[1]

        # +2 proto, protože na stranách matice nesmí být překážka
        height = height + 2
        width = width + 2

        return int(round(height)), int(round(width))

    def __create_line_between_nodes(self, point1, point2, matrix, fill):
        """
        Spojí dva body point1 a point2 přímkou a zapíše ji do matice(matrix), kterou vrátí
        """
        point1[0] = int(round(point1[0]))
        point1[1] = int(round(point1[1]))
        point2[0] = int(round(point2[0]))
        point2[1] = int(round(point2[1]))

        priority = {
            "UpR": ((point1[1] < point2[1]) and (point1[0] > point2[0])),
            "R"  : ((point1[1] < point2[1]) and (point1[0] == point2[0])),
            "DwR": ((point1[1] < point2[1]) and (point1[0] < point2[0])),
            "D"  : ((point1[1] == point2[1]) and (point1[0] < point2[0])),
            "DwL": ((point1[1] > point2[1]) and (point1[0] < point2[0])),
            "L"  : ((point1[1] > point2[1]) and (point1[0] == point2[0])),
            "LUp": ((point1[1] > point2[1]) and (point1[0] > point2[0])),
            "Up" : ((point1[1] == point2[1]) and (point1[0] > point2[0])),
        }
        """Priority zjístí v jaké pozici je point1 vzhledem k bodu point2. "UpR"- point2 je vpravo nahoře od bodu point1 ..."""


        matrix[ point1[0] ][ point1[1] ] = fill
        matrix[ point2[0] ][ point2[1] ] = fill

        move_point = point1

        delta_x = abs(point1[1] - point2[1])  # zjístí rozdíl mezi body v x-ové souřadnici
        delta_y = abs(point1[0] - point2[0])  # zjístí rozdíl mezi body v y-ové souřadnici

        if(delta_y == 0):
            delta = 0                         # při pohybu vpravo nebo vlevo je y-ová souřadnice stejná a nemůžeme dělit nulou
        else:
            delta = delta_x / delta_y         # zjístí o kolik se má posunout v x-ové souřadnici aby se mohl posunout o jednu y- ovou souřadnici

        shift = math.ceil(delta)              # pro snazší algoritmus zaokrouhlíme nahoru
        delta_shift = shift - delta           # vypočítáme rozdíl mezi zaokrouhleným posunem a reálným posunem
                                              # delta_shift určije kdy se má snížit posun v x-ové souřadnici vzhledem k zaokrouhlení nahoru (vždy posouvám x-ovou souřadnici více než je reálný poměr mezi x a y, a proto musím jendou za několik 'kol' snížit posun xové souřadnice)


        if (priority["UpR"]):                 # určení jak se má point1(move_point) posouvat
            zmena = 0                         # určuje kdy se má snážit posun x- ové složky
            odecitac = 0                      # provede snížení
            while(move_point != point2):      #prováděj tak dlouho dokud se bod1 nepropojí s bodem2
                move_point[0] = move_point[0] -1               # move up
                matrix[move_point[0]][move_point[1]] = fill    # set point as edge of environment(as 1 (False is free space))
                zmena = zmena + delta_shift + 0.00000001            # počítá odchylku od zaokrouhleného poměru mezi x a y a reálného x a y (příčtení malého čísla kvůli periodickým číslum 0.999999999)

                if(zmena >= 1):             # pokud je změna větší nebo rovna jedné, sníží se posun v x ové složce
                    zmena = zmena -1
                    odecitac = 1            # provede snížení v nejbližším for cyklu
                else:
                    odecitac = 0

                for i in range(0, (int(shift)-odecitac)):       # posune bod shift krát doprava (někdy shift - 1 doprava)
                    move_point[1] = move_point[1] + 1           # move right
                    matrix[move_point[0]][move_point[1]] = fill # doplní do matice bod


        elif(priority["R"]):
            while(move_point != point2):
                move_point[1] = move_point[1] + 1           # move right
                matrix[move_point[0]][move_point[1]] = fill

        elif(priority["DwR"]):
            zmena = 0
            odecitac = 0
            while(move_point != point2):
                zmena = zmena + delta_shift + 0.00000001

                if(zmena >= 1):
                    zmena = zmena -1
                    odecitac = 1
                else:
                    odecitac = 0

                for i in range(0, (int(shift)-odecitac)):
                    move_point[1] = move_point[1] + 1   # move right
                    matrix[move_point[0]][move_point[1]] = fill


                move_point[0] = move_point[0] +1   # move down
                matrix[move_point[0]][move_point[1]] = fill    # set point as edge of environment(as 1 (False is free space)

        elif(priority["D"]):
            while(move_point != point2):
                move_point[0] = move_point[0] + 1           # move down
                matrix[move_point[0]][move_point[1]] = fill

        elif(priority["DwL"]):
            zmena = 0
            odecitac = 0
            while(move_point != point2):
                move_point[0] = move_point[0] +1   # move down
                matrix[move_point[0]][move_point[1]] = fill    # set point as edge of environment(as 1 (False is free space))
                zmena = zmena + delta_shift + 0.00000001

                if(zmena >= 1):
                    zmena = zmena -1
                    odecitac = 1
                else:
                    odecitac = 0

                for i in range(0, (int(shift)-odecitac)):
                    move_point[1] = move_point[1] - 1   # move right
                    matrix[move_point[0]][move_point[1]] = fill

        elif(priority["L"]):
            while(move_point != point2):
                move_point[1] = move_point[1] - 1           # move left
                matrix[move_point[0]][move_point[1]] = fill

        elif(priority["LUp"]):
            zmena = 0
            odecitac = 0
            while(move_point != point2):
                zmena = zmena + delta_shift + 0.00000001

                if(zmena >= 1):
                    zmena = zmena - 1
                    odecitac = 1
                else:
                    odecitac = 0

                for i in range(0, (int(shift)-odecitac)):
                    move_point[1] = move_point[1] - 1   # move left
                    matrix[move_point[0]][move_point[1]] = fill

                move_point[0] = move_point[0] -1   # move up
                matrix[move_point[0]][move_point[1]] = fill    # set point as edge of environment(as 1 (0 is free space))

        elif(priority["Up"]):
            while(move_point != point2):
                move_point[0] = move_point[0] - 1           # move down
                matrix[move_point[0]][move_point[1]] = fill

        return matrix

    def __flood_fill(self, init_point):
        
        number = 3          # start with 3 because 1 represent edge, 2 represent barriers, 0 represent free space
        self.__matrix[init_point[0]][init_point[1]] = number

        while(True):
            found = False

            for y in range(self.__matrix.shape[0]):
                for x in range(self.__matrix.shape[1]):

                    if(self.__matrix[y][x] == number):

                        if((x+1) < self.__matrix.shape[1] and (self.__matrix[y][x+1] != (self.__edge_space) and (self.__matrix[y][x+1] == self.__free_space))):
                            self.__matrix[y][x+1] = number + 1
                            found = True

                        if((x-1) >= 0 and (self.__matrix[y][x-1] != (self.__edge_space)) and (self.__matrix[y][x-1] == self.__free_space)):

                            self.__matrix[y][x-1] = number + 1
                            found = True

                        if((y+1) < self.__matrix.shape[0] and (self.__matrix[y+1][x] != (self.__edge_space)) and (self.__matrix[y+1][x] == self.__free_space)):

                            self.__matrix[y+1][x] = number + 1
                            found = True

                        if((y-1) >= 0 and (self.__matrix[y-1][x] != (self.__edge_space)) and (self.__matrix[y-1][x] == self.__free_space)):
                                self.__matrix[y-1][x] = number + 1
                                found = True

            number = number + 1

            if found == False:
                break

    def __do_halfline(self,point):
        """
        Vytvoří polopřímku doprava a dolů
        """
        xline = []
        yline = []

        for y in range(point[0], self.__matrix.shape[0]):
            yline.append([y,point[1]])

        for x in range(point[1], self.__matrix.shape[1]):
            xline.append([point[0],x])

        return yline, xline

    def __point_in_barrier(self, point):
        """
        vráti tru nebo false pokud point je uzel bariery
        """

        state = False
        for p in self.__list_of_barriers:
            iterator = 0
            if(p != []):
                while(iterator != (len(p)-1)):
                    point1 = p[iterator]
                    if(point1 == point):
                        return True             # bod je mezi barierami

                    iterator = iterator + 1

        return False

    def __complete_environment(self, matrix):
        """
        Vyplnění prostoru, který je vně prostoru, který prohledávám (algoritmus flood fill (upravený))
        na souřadnici x = 0 a y = 0 nemůže být překžka, a proto je vždycky bod [0,0] vně prostoru a algoritmus začíná od něj
        """

        self.__flood_fill([0,0])

        for y in range(1,self.__matrix.shape[0]-1):
            for x in range(1,self.__matrix.shape[1]-1):

                if(self.__matrix[y][x] == 0):
                    y_halfline, x_halfline = self.__do_halfline([y,x])
                else:
                    continue

                barrier_x = 0
                barrier_y = 0

                start = False
                for point in y_halfline:
                    if(self.__matrix[point[0]][point[1]] == self.__barrier_space):
                        if(not start):
                            barrier_y = barrier_y + 1
                            start = True

                            if(self.__point_in_barrier(point)):
                                barrier_y = 0
                                break
                    else:
                        start = False


                start = False
                state = False
                for point in x_halfline:
                    if(self.__matrix[point[0]][point[1]] == 2):
                        if(not start):
                            barrier_x = barrier_x + 1
                            start = True

                            if(self.__point_in_barrier(point)):
                                barrier_x = 0
                                break

                    else:
                        start = False


                if((barrier_x % 2 != 0) and (barrier_y % 2 != 0) and (barrier_x != 0) and (barrier_y != 0)):
                    self.__flood_fill([y,x])



        for y in range(self.__matrix.shape[0]):
            for x in range(self.__matrix.shape[1]):
                if(self.__matrix[y][x] != (0 )):
                    self.__matrix[y][x] = self.__edge_space


    def create_edge_and_barriers(self, edge, barriers, matrix):
        """
        Vrátí matici s celým prostředím (hranice a překážky uvnitř)
        """
        iterator = 0
        while(iterator != (len(edge)-1)):
            point1 = edge[iterator]
            point2 = edge[iterator + 1]

            matrix = self.__create_line_between_nodes(point1, point2, matrix, self.__edge_space)

            iterator = iterator + 1


        for point in barriers:
            iterator = 0
            if(point != []):
                while(iterator != (len(point)-1)):
                    point1 = point[iterator]
                    point2 = point[iterator + 1]

                    matrix = self.__create_line_between_nodes(point1, point2, matrix, self.__barrier_space)

                    iterator = iterator + 1

        self.__complete_environment(matrix)

        return matrix
