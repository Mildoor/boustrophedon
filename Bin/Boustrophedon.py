class Boustrophedon():
    """
    Vypočítá body jednotlivých buněk(Boustrophedon dekompozice) a vrátí je v nejkratším pořadí, tak aby cesta pro robota byla nejkratší
    Vstupem je matice (prostředí) s překážkami
    """
    def __init__(self, matrix):
        self.__matrix = matrix
        self.__edge_space = 1   # reprezentace překážky v matici
        self.__free_space = 0   # reprezentace průjezdného prostoru v překážce

        self.__line_number = 0  # souřadnice právě prohledáváne překážky

        self.__line_one = []    # pole v předchozím kroku kontrolované linie
        self.__line_two = []    # pole právě kontrolované linie

        self.__cell_number = 1  # nejvyšší bunka
        self.__pervious_cell_list = []  # pořadí buněk v předchozím kroku

        self.__obstacles_one = 0    # počet překážek v lini jedna
        self.__obstacles_two = 0    # počet překážek v linii dva (aktuálně prohledávané)
        self.__obstacles_one_list = []  # souřadnice překážek v linii jedna
        self.__obstacles_two_list = []  # souřadnice překážek v linii dvě

        self.__cell_one = self.__obstacles_one - 1  # počet lokálních bunek v linii jedna
        self.__cell_two = self.__obstacles_two - 1  # počet lokálních bunek v linii dva
        self.__cell_one_list = []           # souřadnice lokálních buněk v linii 1
        self.__cell_two_list = []           # souřadnice lokálních bunek v linii 2

        self.__delta_of_obstacles = self.__obstacles_two - self.__obstacles_one

        self.__discontinuity = []           # číslo překážky (v linii 2), která není spojitá s překážkami z linie jedna
        self.__relative_continuity = False  # True - každá překážka v linii dva je spojitá se některou z překážek v lini jedna
        self.__absolute_continuity = False  # True - první(linie 1) je spojitá s první (linie2) překážkou,  druhá s druhou ...


        """
        Stav ve kterém se vyskytuje linie 2
        """
        self.__state = {
                "Beginning": ( self.__obstacles_two >= 1 and self.__cell_one == 0),
                "MoreObst_Discont": ( self.__obstacles_two >= self.__obstacles_one and self.__cell_one != 0 and not self.__relative_continuity),
                "MoreObst_RelContin": ( self.__obstacles_two >= self.__obstacles_one and self.__cell_one != 0 and self.__relative_continuity),
                "EquObst_AbsContin": (self.__obstacles_one == self.__obstacles_two and self.__absolute_continuity == True and self.__cell_two != 0  ),
                "LessObst": (self.__obstacles_two < self.__obstacles_one),
                "End": (self.__obstacles_one != 0 and self.__obstacles_two == 0)
                }

    @property
    def cell_number(self):
        return  self.__cell_number

    def __get_next_lines(self):
        """
        Zvýší pozicí obou linií
        """
        if(self.__line_number >= self.__matrix.shape[1]):
            self.__line_one = self.__line_two
        else:
            self.__line_one = self.__matrix[:, self.__line_number - 1]
            self.__line_two = self.__matrix[:, self.__line_number]


    def number_of_obstacles(self, list):
        """
        Vrátí počet překážek v poli list
        """
        number = 0
        control_one = False
        control_two = False

        for i in list:
            control_two = control_one
            if(i == self.__edge_space):
                control_one = True
            else:
                control_one = False

            if((control_one and (not control_two))):
                number = number + 1

        return number


    def coordinate_of_obstacles(self,list):
        """
        Vrátí pole krajních souřadnic všech překážek v poli list
        """
        return_list = []
        sublist = []

        number = 0
        control_one = False
        control_two = False

        for i in list:
            control_two = control_one
            if(i == self.__edge_space):
                control_one = True
            else:
                control_one = False

            if((control_one and (not control_two))):
                sublist.append(number)

            elif(((not control_one) and control_two)):
                sublist.append(number-1)
                return_list.append(sublist)
                sublist = []

            number = number + 1

        sublist.append(number-1)
        return_list.append(sublist)

        return return_list

    def coordinate_of_cells(self, list):
        """
        Vrátí pole krajních souřadnic všech buněk v poli list
        """
        return_list = []
        sublist = []

        number = 0
        control_one = False
        control_two = False

        for i in list:
            control_two = control_one
            if(i != self.__edge_space):
                control_one = True
            else:
                control_one = False

            if((control_one and (not control_two))):
                sublist.append(number)

            elif(((not control_one) and control_two)):
                sublist.append(number-1)
                return_list.append(sublist)
                sublist = []

            number = number + 1

        return return_list

    def absolute_continuity(self, list_one, list_two):
        """
        Kontroluje zda druhá linie(list_two) je spojitá absolutně s linií jedna (list_one)
        """
        if(len(list_one) != len(list_two)):
            return False
        else:
            iterator = 0

            while(iterator < len(list_two)):

                if(( (list_one[iterator][0] <= list_two[iterator][0]) and (list_two[iterator][0] <= list_one[iterator][1])) or ( (list_one[iterator][0] <= list_two[iterator][1]) and (list_two[iterator][1]<= list_one[iterator][1])) ):
                    pass
                elif(( (list_two[iterator][0] <= list_one[iterator][0]) and (list_one[iterator][0] <= list_two[iterator][1])) or ( (list_two[iterator][0] <= list_one[iterator][1]) and (list_one[iterator][1]<= list_two[iterator][1])) ):
                    pass
                else:
                    return False

                iterator = iterator + 1
        return True

    def relative_continuity(self, list_one, list_two):
        """
        Kontroluje zda druhá linie je relativně spojitá s linii jedna
        """
        connection = []
        sublist = []

        for i in range(len(list_one)):
            for j in range(len(list_two)):

                if(( (list_one[i][0] <= list_two[j][0]) and (list_two[j][0] <= list_one[i][1])) or ( (list_one[i][0] <= list_two[j][1]) and (list_two[j][1] <= list_one[i][1])) ):
                    sublist = [i,j]
                elif(( (list_two[j][0] <= list_one[i][0]) and (list_one[i][0] <= list_two[j][1])) or ( (list_two[j][0] <= list_one[i][1]) and (list_one[i][1]<= list_two[j][1])) ):
                    sublist = [i,j]
                if(sublist != []):
                    connection.append(sublist)
                    sublist = []

        loop1 = True
        loop2 = True
        for i in range(len(list_one)):
            for j in range(len(connection)):

                if( i == connection[j][0]):
                    loop1 = True
                    break
                else:
                    loop1 = False

            if(not loop1):
                break


        for i in range(len(list_two)):
            for j in range(len(connection)):

                if( i == connection[j][1]):
                    loop2 = True
                    break
                else:
                    loop2 = False

            if(not loop2):
                break


        if(loop1 and loop2):
            return True
        else:
            return False

    def discontinuous_sets(self, list_one, list_two):
        """
        Vrátí překážky(jejich pozici v rámci linie) v linii, které jsou nespojité
        """
        connection = []
        sublist = []

        for i in range(len(list_one)):
            for j in range(len(list_two)):


                if(( (list_one[i][0] <= list_two[j][0]) and (list_two[j][0] <= list_one[i][1])) or ( (list_one[i][0] <= list_two[j][1]) and (list_two[j][1] <= list_one[i][1])) ):
                    sublist = [i,j]
                elif(( (list_two[j][0] <= list_one[i][0]) and (list_one[i][0] <= list_two[j][1])) or ( (list_two[j][0] <= list_one[i][1]) and (list_one[i][1]<= list_two[j][1])) ):
                    sublist = [i,j]
                if(sublist != []):
                    connection.append(sublist)
                    sublist = []

        discontinuity = []
        set = True
        for i in range(len(list_two)):
            for j in range(len(connection)):

                if(i == connection[j][1]):
                    set = True
                    break
                else:
                    set = False

            if(not set):
                discontinuity.append(i)

        return discontinuity

    def fill_cell(self, number_of_cell, fill):
        """
        Vyplní lokální bunku (number_of_cell -tou shora) číslem(fill)
        """
        for i in range(self.__cell_two_list[number_of_cell][1] - self.__cell_two_list[number_of_cell][0] + 1):
            self.__matrix[self.__cell_two_list[number_of_cell][0] + i][self.__line_number] = fill


    def update(self):
        """
        Aktualizuje stav obou linii po jejich kontrole ve funkci final_decomposition
        """
        self.__line_number = self.__line_number + 1
        self.__get_next_lines()

        self.__obstacles_one = self.number_of_obstacles(self.__line_one)
        self.__obstacles_two = self.number_of_obstacles(self.__line_two)

        self.__obstacles_one_list = self.coordinate_of_obstacles(self.__line_one)
        self.__obstacles_two_list = self.coordinate_of_obstacles(self.__line_two)

        self.__cell_one = self.__obstacles_one - 1
        self.__cell_two = self.__obstacles_two - 1

        self.__cell_one_list = self.coordinate_of_cells(self.__line_one)
        self.__cell_two_list = self.coordinate_of_cells(self.__line_two)

        self.__delta_of_obstacles = self.__obstacles_two - self.__obstacles_one


        self.__discontinuity = self.discontinuous_sets(self.__obstacles_one_list, self.__obstacles_two_list)
        self.__relative_continuity = self.relative_continuity(self.__obstacles_one_list, self.__obstacles_two_list)
        self.__absolute_continuity = self.absolute_continuity(self.__obstacles_one_list, self.__obstacles_two_list)

        self.__state = {
                "Beginning": ( self.__obstacles_two >= 1 and self.__cell_one == 0),
                "MoreObst_Discont": ( self.__obstacles_two >= self.__obstacles_one and self.__cell_one != 0 and not self.__relative_continuity),
                "MoreObst_RelContin": ( self.__obstacles_two >= self.__obstacles_one and self.__cell_one != 0 and self.__relative_continuity),
                "EquObst_AbsContin":    (self.__obstacles_one == self.__obstacles_two and self.__absolute_continuity and self.__cell_two != 0  ),
                "LessObst": (self.__obstacles_two < self.__obstacles_one),
                "End": (self.__obstacles_one != 0 and self.__obstacles_two == 0)
                }

    def is_cell_continuity(self, cellone, celltwo):

        if(( (cellone[0] <= celltwo[0]) and (celltwo[0] <= cellone[1])) or ( (cellone[0] <= celltwo[1]) and (celltwo[1] <= cellone[1])) ):
            return True
        elif(( (celltwo[0] <= cellone[0]) and (cellone[0] <= celltwo[1])) or ( (celltwo[0] <= cellone[1]) and (cellone[1]<= celltwo[1])) ):
            return True

        return False

    def final_decomposition(self):
        """
        Vykoná Boustrophedon dekompozici (rozdělí prostor na buňky)
        """
        while(self.__line_number < self.__matrix.shape[1]-1):

            self.update()

            if(self.__state["EquObst_AbsContin"]):            #
                for i in range(len(self.__pervious_cell_list)):
                    self.fill_cell(i, self.__pervious_cell_list[i])


            elif(self.__state["MoreObst_Discont"]):       # Více nebo stejně překážek, které jsou navzájem nespojité

                one = True
                sec = False
                actual = []
                num = 0

                for i in range(1, self.__obstacles_two):
                    sec = one
                    for j in self.__discontinuity:          # tady zjišťuji nespojitost
                        if(i == j):
                            one = False
                            break
                        else:
                            one = True

                    if(sec and one ):       # pokud je spojitá první i druhá překážka v linii dva

                        if(self.is_cell_continuity(self.__cell_one_list[num], self.__cell_two_list[i-1])):
                            jump = 0
                            for k in range(num, self.__cell_one):
                                if(self.__cell_two_list[i-1][1] >= self.__cell_one_list[k][0]):
                                    jump = jump + 1

                            if(jump == 1):
                                self.fill_cell(i-1, self.__pervious_cell_list[num])
                                actual.append(self.__pervious_cell_list[num])
                            else:

                                self.__cell_number = self.__cell_number + 1
                                self.fill_cell(i-1, self.__cell_number)
                                actual.append(self.__cell_number)

                            num = num + jump

                        elif(self.__cell_two_list[i-1][1] < self.__cell_one_list[num][0]):
                            self.__cell_number = self.__cell_number + 1
                            actual.append(self.__cell_number)               # přiřadí ji do nového seznamu
                            self.fill_cell(i-1, self.__cell_number)

                    else:   # když dvě posobě jdoucí překážky nejsou spojíté
                        self.__cell_number = self.__cell_number + 1     # přířadí novou buňku
                        actual.append(self.__cell_number)               # přiřadí ji do nového seznamu
                        self.fill_cell(i-1, self.__cell_number)         #  A vyplní ji

                        for k in range(num, self.__cell_one):
                            if(self.__cell_two_list[i-1][1] >= self.__cell_one_list[k][0]):
                                num = num + 1

                self.__pervious_cell_list = actual

            elif(self.__state["MoreObst_RelContin"]):   # více překážek, které jsou relativně spojíté
                disc_cell = self.discontinuous_sets(self.__cell_one_list, self.__cell_two_list)
                discontinuous_cell = False

                for i in range(self.__cell_two):
                    for j in disc_cell:
                        if(i == j):
                            discontinuous_cell = True
                            break
                        else:
                            discontinuous_cell = False

                    if(discontinuous_cell):
                        self.__cell_number = self.__cell_number + 1
                        self.__pervious_cell_list.insert(i, self.__cell_number)
                        self.fill_cell(i, self.__pervious_cell_list[i])
                    else:
                        self.fill_cell(i, self.__pervious_cell_list[i])


            elif(self.__state["LessObst"]): # méně překážek
                one = True
                sec = False
                actual = []
                num = 0
                add = 0

                for i in range(1,self.__obstacles_two):
                    newadd = 0
                    sec = one
                    for j in self.__discontinuity:  # zjístí spojitost
                        if(i == j):
                            one = False
                            break
                        else:
                            one = True

                    for k in range(num, len(self.__cell_one_list)):
                        cell = self.__cell_one_list[k]
                        if(cell[1] < self.__cell_two_list[i-1][0]):
                            if(add + 1 <= (self.__cell_one - self.__cell_two)):
                                add = add + 1
                                newadd = newadd + 1

                    if(sec and one ):
                        end_obst = 0
                        for obst in self.__obstacles_one_list:
                            if( obst[0] > self.__obstacles_two_list[i-1][1] and obst[1] < self.__obstacles_two_list[i][0]  ):
                                end_obst = end_obst + 1
                                if( obst[0] > self.__obstacles_two_list[i][1]):
                                    break

                        if(end_obst == 0):
                            num = num + newadd
                            self.fill_cell(i-1, self.__pervious_cell_list[num])  # i-1 protože se vyplnuji bunku (cell) a ta má o jedno nižší číslo
                            actual.append(self.__pervious_cell_list[num])
                            num = num + 1
                            end_obst = 0
                        else:
                            num = num + newadd + end_obst + 1
                            self.__cell_number = self.__cell_number + 1
                            self.fill_cell(i-1, self.__cell_number)
                            actual.append(self.__cell_number)
                            end_obst = 0

                    else:
                        self.__cell_number = self.__cell_number + 1
                        actual.append(self.__cell_number)
                        self.fill_cell(i-1, self.__cell_number)


                        for k in range(num, self.__cell_one):
                            if(self.__cell_two_list[i-1][1] >= self.__cell_one_list[k][0]):
                                num = num + 1
            

                self.__pervious_cell_list = actual

            elif(self.__state["Beginning"]):        # počáteční ustanovení

                if(self.__cell_two >= 1):
                    for i in range(self.__cell_two):
                        self.__cell_number = self.__cell_number + 1
                        self.__pervious_cell_list.append(self.__cell_number)
                        self.fill_cell(i, self.__cell_number)

        return self.__matrix
