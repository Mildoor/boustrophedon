class Create_point():
    def __init__(self, matrix, sequence_cell, node_edge_point, node_barrier_point):
        self.__matrix = matrix
        self.__sequence_cell = sequence_cell

        self.__node_edge_point = node_edge_point
        self.__node_barrier_point = node_barrier_point

        self.__sequence_point = []

        self.__line_one = []
        self.__line_number = 0

        self.__state = {
                "start": True,
                "increase":  False,
                "decrease":  False,
                "zero": False,
                "invert": False
        }


    @property
    def sequence_point():
        return self.__sequence_point

    def __get_next_lines(self, inverse = False):
        """
        Zvýší pozicí obou linií
        """
        if(not inverse):
            self.__line_number = self.__line_number + 1
            if(self.__line_number >= self.__matrix.shape[1]):
                pass
            else:
                self.__line_one = self.__matrix[:, self.__line_number]

        elif(inverse):
            self.__line_number = self.__line_number - 1
            if(self.__line_number <= 0):
                pass
            else:
                self.__line_one = self.__matrix[:, self.__line_number]


    def __number_in_line(self, line, number):
        """
        Zjístí zda hodnota bunky se nachází v kontrolované linii
        """
        for i in line:
            if(i == number):
                return True
        return False

    def __search_cell(self, number_of_cell):
        """
        Vrátí xovou souřadnici linie, kde začíná bunka
        """
        iterator = 0
        self.__line_number = 0
        while(iterator <= self.__matrix.shape[1]):
            iterator = iterator + 1

            self.__get_next_lines()
            if(self.__number_in_line(self.__line_one, number_of_cell)):
                mc = self.__line_number
                self.__line_number = 0
                return mc

        return False

    def __in_cell(self, number):
        """
        Vrátí False pokud je linie_two mimo a linie_one v
        """
        if( self.__number_in_line(self.__line_one, number)):
            return True
        else:
            return False

    def __return_point(self, line, number, mod):
        """
        Vrátí y souřadnici bodu, musíme zadat mod: direct nebo inverse nebo transition
        """
        index = 0
        if(mod == "direct"):
            for y in line:
                if(y == number):
                    return [(index-1), self.__line_number]

                index = index + 1

        elif(mod == "inverse"):
            cell = False
            in_cell = False
            for y in line:
                if(y == number):
                    cell = True
                    in_cell = True
                else:
                    cell = False

                if(not cell and in_cell):
                    return [index, self.__line_number]

                index = index + 1
    def is_important(self, point, search_point):
        write = None
        if(point == []):
            pass
        else:
            if(point == search_point):
                write = point
            elif(point == [search_point[0]+1,search_point[1]-1]):
                write = point
            elif(point == [search_point[0]+1,search_point[1]  ]):
                write = point
            elif(point == [search_point[0]+1,search_point[1]+1]):
                write = point
            elif(point == [search_point[0]  ,search_point[1]-1]):
                write = point
            elif(point == [search_point[0]  ,search_point[1]+1]):
                write = point
            elif(point == [search_point[0]-1,search_point[1]-1]):
                write = point
            elif(point == [search_point[0]-1,search_point[1]  ]):
                write = point
            elif(point == [search_point[0]-1,search_point[1]+1]):
                write = point

        return write

    def find_point(self):
        list = []
        for cell in self.__sequence_cell:
            cell = cell + 1
            sublist = []

            self.__line_number = self.__search_cell(cell)
            mc = self.__return_point(self.__line_one, cell, "inverse")
            w = True
            for point in self.__node_edge_point:
                write = self.is_important(point, mc)
                if(write != None):
                    sublist.append(write)
                    w = False
                    break

            if(w):
                for point in self.__node_barrier_point:
                    write = self.is_important(point, mc)

                    if(write != None):
                        sublist.append(write)
                        break

            if(len(sublist) == 0):
                sublist.append(mc)


            mc = self.__return_point(self.__line_one, cell, "direct")
            w = True
            for point in self.__node_edge_point:
                write = self.is_important(point, mc)

                if(write != None):
                    sublist.append(write)
                    w = False
                    break

            if(w):
                for point in self.__node_barrier_point:
                    write = self.is_important(point, mc)

                    if(write != None):
                        sublist.append(write)
                        break

            if(len(sublist) == 1):
                sublist.append(mc)

            self.__get_next_lines()


            while(self.__in_cell(cell)):
                mc = self.__return_point(self.__line_one, cell, "direct")
                w = True
                for point in self.__node_edge_point:
                    write = self.is_important(point, mc)

                    if(write != None):
                        if(write != sublist[-1]):
                            sublist.append(write)
                            w = False
                            break

                if(w):
                    for point in self.__node_barrier_point:
                        write = self.is_important(point, mc)

                        if(write != None):
                            if(write != sublist[-1]):
                                sublist.append(write)
                                break

                self.__get_next_lines()


            self.__get_next_lines(True)

            len1 = len(sublist)
            w = True
            mc = self.__return_point(self.__line_one, cell, "direct")
            for point in self.__node_edge_point:
                write = self.is_important(point, mc)
                if(write != None):
                    if(sublist[-1] == write):
                        len1 = len1 + 1
                        w = False
                        break
                    elif(sublist[-1] != write):
                        sublist.append(write)
                        w = False
                        break

            if(w):
                for point in self.__node_barrier_point:
                    write = self.is_important(point, mc)

                    if(write != None):
                        if(sublist[-1] == write):
                            len1 = len1 + 1
                            break
                        elif(sublist[-1] != write):
                            sublist.append(write)
                            break

            if(len1 == len(sublist)):
                sublist.append(mc)

            len1 = len(sublist)
            w = True
            mc = self.__return_point(self.__line_one, cell, "inverse")
            for point in self.__node_edge_point:
                write = self.is_important(point, mc)

                if(write != None):
                    if(sublist[-1] == write):
                        len1 = len1 + 1
                        w = False
                        break
                    elif(sublist[-1] != write):
                        sublist.append(write)
                        w = False
                        break

            if(w):
                for point in self.__node_barrier_point:
                    write = self.is_important(point, mc)

                    if(write != None):
                        if(sublist[-1] == write):
                            len1 = len1 + 1
                            break
                        elif(sublist[-1] != write):
                            sublist.append(write)
                            break

            if(len1 == len(sublist)):
                sublist.append(mc)


            while(self.__in_cell(cell)):
                mc = self.__return_point(self.__line_one, cell, "inverse")
                w = True
                for point in self.__node_edge_point:
                    write = self.is_important(point, mc)
                    if(write != None):
                        if(write != sublist[-1]):
                            sublist.append(write)
                            w = False
                            break

                if(w):
                    for point in self.__node_barrier_point:
                        write = self.is_important(point, mc)

                        if(write != None):
                            if(write != sublist[-1]):
                                sublist.append(write)
                                break

                self.__get_next_lines(True)

            if(sublist[0] == sublist[-1]):
                del sublist[-1]

            list.append(sublist)


        return list
