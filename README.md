# Boustrophedon
Boustrophedon cell decomposition is a method used for decomposing given map into cell regions that can be later used for path planning.
We implemented DFS and BFS algorithms to calculate the sequence at which said cells should be searched.
The output of our program is represented in graphical interface with obstacles colored black, free space with cell indexes colored white and cell nodes colored red.

## What you need
To use this program, you first need to download Python 3.7 (https://www.python.org/).
We use numpy library that can be downloaded here: https://www.numpy.org/. To skip this step, we recommend downloading Anaconda, because all the libraries are already provided. Link: https://www.anaconda.com/distribution/

## User guide

**All file expected positive coordinates in meters!!!**

To make this work, first you have to guide the program to your node files by adding a path into **Start_gui.py** (with graphical interface) or **Final.py** (with graphical interface) file.

Maximum value edge in graphical interface is 7 meters.

### Input
This program expects two files:
* File (**hranice.csv**) with edge nodes
    >Each coordinate separated by a comma as shown in the example file hranice.csv. **The first coordinate must be the same as the last one because of our implementation!**

* File (**prekazka.csv**) with obstacle nodes
    >Same formatting as the edge nodes, example shown in the prekazky.csv file.


```python
rot = Rotation.Rotation("C:\\ ... \\hranice.csv",
                        "C:\\ ... \\prekazka.csv")
```

### Output
The output of this program is in a form of matrices.

* First output is matrix called **sequence**
    >This matrix contains cell sequence calculated by DFS algorithm. You can choose other searching algorithms.
    ```python
    sequence = way.DFS_Algorithm()
    ```
    or
    ```python
    sequence = way.BFS_Algorithm()
    ```

* First output is 3D matrix called **final_points**
    >Final_points is 3 dimensional matrix that contains node coordinates of each cell in a clockwise arrangement.
    Example
    ```python
    final_points = [
    [[5.0, 0.1], [0.1, 0.1], [0.1, 0.7], [5.0, 0.7], [5.0, 0.1]],
    [[3.5, 0.8], [0.1, 0.8], [0.1, 0.9], [3.5, 0.8]],
    [[1.0, 1.0], [0.1, 1.0], [0.1, 2.5], [0.3, 2.5], [1.0, 1.0]],
    [[3.0, 1.0], [2.6, 1.0], [2.6, 3.5], [2.6, 3.5], [3.0, 3.5], [3.0, 1.0]],
    [[3.0, 3.6], [1.5, 3.6], [1.5, 3.8], [1.5, 3.8], [3.0, 3.8]],
    [[2.3, 2.6], [1.5, 2.6], [1.5, 3.5], [2.6, 3.5]],
    [[1.5, 2.0], [1.3, 2.0], [0.8, 2.5], [1.8, 2.5]],
    [[1.3, 2.6], [0.7, 2.6], [0.1, 3.0], [0.1, 3.8], [1.3, 3.8], [0.8, 3.0],  [1.3, 2.6]],
    [[3.0, 4.0], [0.1, 4.0], [3.0, 4.0]],
    [[2.2, 2.2], [2.0, 2.2], [1.8, 2.5], [1.9, 2.5], [2.2, 2.5]],
    [[3.5, 1.0], [3.2, 1.0], [3.2, 2.4], [3.5, 2.4]],
    [[3.7, 2.4], [3.2, 2.5], [3.2, 3.0], [3.8, 3.0], [3.8, 2.5], [3.7, 2.4]],
    [[5.0, 3.1], [3.2, 3.1], [3.2, 4.0], [3.2, 3.9], [5.0, 4.0]],
    [[4.2, 2.5], [4.0, 2.5], [4.0, 3.0], [4.0, 3.0], [4.2, 3.0], [4.2, 2.5]],
    [[4.6, 1.1], [3.7, 1.1], [3.7, 2.4], [3.8, 2.5], [3.7, 2.4], [4.6, 2.4], [4.6, 1.1]],
    [[5.0, 0.8], [3.7, 0.8], [3.7, 1.0], [5.0, 1.0]],
    [[5.0, 1.1], [4.8, 1.1], [4.8, 2.7], [4.8, 2.7], [5.0, 2.7]],
    [[5.0, 2.8], [4.4, 2.8], [4.4, 3.0], [4.4, 3.0], [5.0, 3.0]],
    [[4.6, 2.5], [4.4, 2.5], [4.4, 2.7], [4.6, 2.7]]
    ]
    ```
